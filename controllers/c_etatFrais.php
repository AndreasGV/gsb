<?php
include("views/v_sommaire.php");
$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
switch($action){
	case 'selectionnerMois':{
		$lesMois=$pdo->getLesMoisDisponibles($idVisiteur);
		// Afin de sélectionner par défaut le dernier mois dans la zone de liste
		// on demande toutes les clés, et on prend la première,
		// les mois étant triés décroissants
		$lesCles = array_keys( $lesMois );
		$moisASelectionner = $lesCles[0];
		include("views/v_listeMois.php");
		break;
	}
	case 'voirEtatFrais':{
		$leMois = $_REQUEST['lstMois']; 
		$lesMois=$pdo->getLesMoisDisponibles($idVisiteur);
		$moisASelectionner = $leMois;
		include("views/v_listeMois.php");
		$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur,$leMois);
		$lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur,$leMois);
		$numAnnee =substr( $leMois,0,4);
		$numMois =substr( $leMois,4,2);
		$libEtat = $lesInfosFicheFrais['libEtat'];
		$montantValide = $lesInfosFicheFrais['montantValide'];
		$nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
		$dateModif =  $lesInfosFicheFrais['dateModif'];
		$dateModif =  dateAnglaisVersFrancais($dateModif);
		include("views/v_etatFrais.php");
		break;
	}
	case 'selectionnerNumVis':{
		$lesNumVis=$pdo->getLesNumVis();
		// requete sql qui recherche les type (modele, appel)
		$lesCles = array_keys( $lesNumVis );
		$numASelectionner = $lesCles[0];
		$lesTypes=$pdo->getLesTypes();
		// requete sql qui recherche les type (modele, appel)
		$leType = $_REQUEST['lstType'];
		$lesCles = array_keys( $lesTypes );
		$typeASelectionner = $lesCles[0];
		include("views/v_fraisVisiteurs.php");
		break;
	}

	case 'voirVisiteurType':{
		$idVisiteur = $_REQUEST['id'];
		$leType = $_REQUEST['type'];
		$numASelectionner = $idVisiteur;
		$lesNumVis=$pdo->getLesNumVis();
		// requete sql qui recherche les type (modele, appel)
	
		$typeASelectionner = $leType;
		$lesTypes=$pdo->getLesTypes();
		//$leType = $_REQUEST['lstType'];
	
		include("views/v_fraisVisiteurs.php");
        $idVisiteur = $_REQUEST['id'];
		$leType = $_REQUEST['type'];
		$lesFraisForfait = $pdo->getLesFraisForfaitType($idVisiteur,$leType);
		include("views/v_visiteurType.php");
		break;
	}


}
